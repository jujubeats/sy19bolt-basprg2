#include <iostream>;
#include <time.h>;

void roll(int& dr1, int& dr2) {
	dr1 = rand() % 6 + 1;
	dr2 = rand() % 6 + 1;
}

void bet(int& tMoney, int& aBet) {

	std::cout << "Bet amount: ";
	std::cin >> aBet;
	std::cout << "\n";

	if (aBet <= tMoney)
	{
		tMoney -= aBet;

		std::cout << tMoney << "\n\n";
	}
}

int main() {
	srand(time(NULL));
	int d1;
	int d2;
	int aBet;
	int pMoney = 1000;
	//int eMoney = 1000;
	int dt[2];
	
	bet(pMoney, aBet);

	for (int i = 0; i < sizeof(dt)/sizeof(dt[0]); i++) {
		roll(d1, d2);
		dt[i] = d1 + d2;
	}

	std::cout << "player roll: " << d1 << " bot roll: " << d2 << "\n\n";

	if (d1 == 2) {
		pMoney += aBet * 3;
	}

	else if (d1 > d2) {
		pMoney += aBet * 2;
	}
	
	std::cout << "player gold: " << pMoney << "\n\n";

	system("pause");
}