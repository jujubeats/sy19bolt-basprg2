#include <iostream>;
#include <time.h>;

void roll(int& dr1, int& dr2) {
	dr1 = rand() % 6 + 1;
	dr2 = rand() % 6 + 1;
}

int main() {
	srand(time(NULL));
	int d1;
	int d2;
	roll(d1, d2);
	std::cout << "d1: " << d1 << "  d2: " << d2 << " Total: " << d1+d2 << "\n\n";

	system("pause");
}