#include <iostream>;
#include <time.h>;

void roll(int& dr1, int& dr2) { // roll part
	dr1 = rand() % 6 + 1;
	dr2 = rand() % 6 + 1;
}

void bet(int& tMoney, int& aBet) { // bet part

	std::cout << "Bet amount: ";
	std::cin >> aBet;
	std::cout << "\n";

	if (aBet <= tMoney)
	{
		tMoney -= aBet;

		//std::cout << tMoney << "\n\n";
	}
}

void playRound(int& pMoney) {
	int d1;        //      
	int d2;        // <---- mess
	int aBet;      //		part
	int dt[2];     //

	bet(pMoney, aBet);                                                    //  <---- S
	                                                                      //        P
	for (int i = 0; i < sizeof(dt) / sizeof(dt[0]); i++) {                //        A
		roll(d1, d2);                                                     //        G
		dt[i] = d1 + d2;                                                  //        H
	}                                                                     //        E
	                                                                      //        T
	std::cout << "player roll: " << d1 << " bot roll: " << d2 << "\n\n";  //        T
																		  //        I
	if (d1 == 2) {                                                        //
		pMoney += aBet * 3;                                               //        P        
	}                                                                     //        A
																		  //        R
	else if (d1 > d2) {                                                   //        T
		pMoney += aBet * 2;                                               //
	}                                                                     //        ;)

	std::cout << "player gold: " << pMoney << "\n\n";
}

int main() {
	srand(time(NULL));
	int pMoney = 1000;
	//int eMoney = 1000;

	while (pMoney>0)
	{
		playRound(pMoney);        //plug and play part
	}

	system("pause");
}