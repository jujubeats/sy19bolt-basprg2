#include <iostream>;

int pBet(int& tMoney) {
	int aBet;

	std::cout << "Bet amount: ";
	std::cin >> aBet;
	std::cout << "\n";

	if (aBet <= tMoney) 
	{
		tMoney -= aBet;

		std::cout << tMoney << "\n\n";
		return aBet;
	} 
}

void pBetwRef(int& tMoney, int& aBet) {

	std::cout << "Bet amount: ";
	std::cin >> aBet;
	std::cout << "\n";

	if (aBet <= tMoney)
	{
		tMoney -= aBet;

		std::cout << tMoney << "\n\n";
	}
}

int main() {
	int aBet = 0;
	int tMoney = 1000;


	aBet = pBet(tMoney);
	system("pause");

	pBetwRef(tMoney,aBet);
	system("pause");
}